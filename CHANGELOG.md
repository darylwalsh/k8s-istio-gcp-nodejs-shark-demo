# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.1.0 (2019-10-31)


### Features

* **gitlab:** migrate repo to gitlab ([bc3ee03](https://gitlab.com/darylwalsh/k8s-istio-gcp-nodejs-shark-demo/commit/bc3ee03b49d9ead7f09982db57eeafb7f76f6503))
